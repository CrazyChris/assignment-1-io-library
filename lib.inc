section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax    
.iterate:
    cmp byte [rdi + rax], 0
    jz .end
    inc rax
    jmp .iterate
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi

    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi        ; put a symbol to stack
    mov rsi, rsp    ; and make this cell a source address
    pop rdi
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rdi, 10   
    xor rcx, rcx

    dec rsp         ; inserting null-terminator
    inc rcx
    mov byte [rsp], 0
    
.push_loop:
    xor rdx, rdx
    div rdi
    add dl, 0x30
    dec rsp
    inc rcx
    mov byte [rsp], dl
    test rax, rax
    jnz .push_loop

    mov rdi, rsp
    push rcx
    call print_string
    pop rcx
    lea rsp, [rsp + rcx]

    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi                
    shl rax, 1                 
    jnc .print

    push rdi
    mov rdi, 0x2D
    call print_char
    pop rax

    neg rax
    mov rdi, rax
    
.print:
    call print_uint

    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
.loop:
    mov al, byte[rdi + rcx]
    cmp al, byte [rsi + rcx]
    jne .error
    cmp al, 0
    je .end
    inc rcx
    jmp .loop

.error:
    xor rax, rax
    ret
.end:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall

    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:    
    push r12              
    push r13                

    push rsi
    push rdi

    mov r12, rsi    ; r12 - remaining space in buffer
    mov r13, rdi    ; r13 - current address in buffer

.whitespace_loop:
    call read_char

    cmp rax, 0x20
    je .whitespace_loop
    cmp rax, 0x9
    je .whitespace_loop
    cmp rax, 0xA
    je .whitespace_loop 

.loop:
    cmp r12, 0
    je .end
    cmp rax, 0
    je .end

    mov byte [r13], al
    inc r13
    dec r12

    call read_char

    cmp rax, 0x20
    je .end
    cmp rax, 0x9
    je .end
    cmp rax, 0xA
    je .end

    jmp .loop

.end:
    pop rax     ; buffer address
    pop rdx     ; buffer size
    sub rdx, r12
    cmp r12, 0
    je .return
    mov byte [r13], 0
    pop r13
    pop r12
    ret
.return:
    xor rax, rax
    pop r13
    pop r12
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor r10, r10
    mov rsi, 10

.loop:
    mov r10b, byte [rdi + rcx]
    cmp r10b, 0x30
    jl .end
    cmp r10b, 0x39
    jg .end

    sub r10b, 0x30
    mul rsi
    add rax, r10
    inc rcx
    jmp .loop

.end:
    mov rdx, rcx

    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], 0x2d
    jne .if_plus
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret
 
.if_plus:
    call parse_uint
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    mov rcx, rdx
.loop:
    cmp rcx, 0
    je .end
    cmp byte [rdi], 0
    je .end
    mov al, byte [rdi]
    mov byte [rsi], al
    inc rdi
    inc rsi
    dec rcx
    jmp .loop

.end:
    cmp rcx, 0
    je .return
    mov byte [rsi], 0
    mov rax, rdx
    sub rax, rcx
    ret
.return:    
    mov rax, 0
    ret
